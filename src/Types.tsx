import { ReactElement } from "react";

export type ColumnType = {
  name: string;
  label: string;
  type: string;
  selectProps?: {
    label: string;
    value: any;
  };
};

export type InputType = {
  column: ColumnType;
  value: any;
  onChange: any;
  validationError: string;
  selectOptions?: any; // Either add them with an API or manually
};

export interface ValidatingFormType {
  id: string;
  onSubmit: () => Promise<void>;
  validationScheme: any; // The yup validation scheme that we'll give to Formik
  inputValues: any; // Our default form state
  validationErrors: any;
  setValidationErrors: (data: any) => void;
  children: JSX.Element;
}

export interface ValidatingFormErrorType {
  message: string;
  path: string;
}

export interface IForm {
  form: any;
  formId: string;
  onSubmit: () => Promise<void>;
  setFormData: any;
  products?: Array<JsonApiDataProducts>; // We might need to call products on our form component
  productsLoading?: boolean; // If we do want to access products then we should check if it's loading as well
}

export type CrudPageType = {
  item: string; // The entity name that we can use in titles, add buttons, etc
  title: string; // The title to show on the list page
  loading: boolean; // Wether the crud page is actually still loading or not
  defaultForm: any; // The crudpage has to be able to reset the form back to it's default
  formData: any; // The edit/add form object should be manipulatable by the crudpage as well
  setFormData: (data: any) => void; // The crudpage should be able to change the form data as well
  openEditForm: (id: number) => Promise<void>; // Call the GET api for this entity to fetch an existing entity and opens the form
  formId: string; // The id of the submit form, so that our CrudPage button can submit it
  form: ReactElement; // Renders the form to be used by the add and edit functionality
  delete: (data: any) => Promise<void>; // Calls the delete method of the parent (to delete an entity)
  listHeaders: Array<ColumnType>; // The columns that we want to see in our list table
  listData: any; // Our list data to be shown in the table Array<ColumnType | null | void | boolean>
  statusMessage: StatusMessageType; // Do we have something to tell to our user?
  modalOpen: boolean; // Should the modal with the add/edit form be open or not
  setModalOpen: (data: boolean) => void; // The setter to open/close the modal
};

/* Types specific to (api) entities */
export interface JsonApiData {
  type: string;
  id: number;
}
export interface JsonApiDataProducts extends JsonApiData {
  attributes: ProductType;
}
export interface JsonApiDataOrders extends JsonApiData {
  attributes: OrderType;
}

export type ProductType = {
  id?: number | null;
  name: string;
  description: string;
  price: number;
  stock: number;
};
export type OrderType = {
  id?: number | null;
  createdAt: string;
  customerName: string;
  shippingAddress: string;
  products: Array<OrderProductsType>;
};
export type OrderProductsType = {
  productId: number;
  quantity: number;
};

/* Types specific for styling */
export type StyledButtonType = {
  text?: string; // A text in the button isn't a requirement as we might only want to show an icon
  icon?: string;
  class?: string; // Doesn't have to be set as the utility can set it for us
  style?: object;
  onClick?: Function;
  form?: string;
  type?: string; // If we need to control this button's type
};

export enum StatusMessages {
  DANGER,
  SUCCESS,
}

export type StatusMessageType = {
  type: StatusMessages | null;
  message: string;
};
