import { Fragment } from "react";
import { InputType } from "../Types";
import { FormGroup, FormControl, FormLabel, Form } from "react-bootstrap";
// @ts-ignore
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

export const Input = (props: InputType) => {
  let formControlProps = {};
  let select: any;

  if (props.column.type == "datepicker") {
    let selectedValue: any = "";
    // "" is the default empty value, null is the value when the user had a date filled in but deletes it
    // So we have to check on both before we momentize the value
    if (props.value != "" && props.value != null) {
      selectedValue = moment(props.value).toDate();
    }

    formControlProps = {
      ...formControlProps,
      as: DatePicker,
      showTimeSelect: true,
      timeFormat: "HH:mm:ss",
      dateFormat: "dd-MM-yyyy HH:mm:ss",
      selected: selectedValue,
      value: selectedValue,
    };
  }

  if (props.column.type == "select") {
    let selectProps = props.column.selectProps;
    if (!selectProps || !props.selectOptions) {
      return (
        <Fragment>
          Could not load the select: no selectProps or no selectOptions
        </Fragment>
      );
    }

    let value = selectProps.value;
    let label = selectProps.label;

    select = (
      <Form.Select
        name={props.column.name}
        value={props.value}
        onChange={props.onChange}
      >
        {props.selectOptions.map((row: any) => {
          return (
            <option key={row[value]} value={row[value]}>
              {row[label]}
            </option>
          );
        })}
      </Form.Select>
    );
  }

  return (
    <FormGroup style={{ marginBottom: 15 }}>
      <FormLabel>{props.column.label}</FormLabel>

      {props.selectOptions && select}

      {!props.selectOptions && (
        <FormControl
          name={props.column.name}
          type={props.column.type}
          value={props.value || ""}
          onChange={props.onChange}
          className={props.validationError ? "is-invalid" : ""}
          {...formControlProps}
        ></FormControl>
      )}
      <FormControl.Feedback type={props.validationError ? "invalid" : "valid"}>
        {props.validationError}
      </FormControl.Feedback>
    </FormGroup>
  );
};
