import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchOrders,
  getOrder,
  createOrder,
  editOrder,
  deleteOrder,
} from "../../redux/orders/OrderActions";
import { OrderType, StatusMessageType, StatusMessages } from "../../Types";
import CrudPage, { defaultApiCatchError } from "../CrudPage";
import { ORDER_FIELDS } from "../../util/Fields";
import { OrderForm } from "./OrderForm";
import { fetchProducts } from "../../redux/products/ProductActions";

let defaultFormState = {
  id: null,
  createdAt: "",
  customerName: "",
  shippingAddress: "",
  products: [{ productId: 1, quantity: 0 }],
};
let formId = "add-or-edit-orders";

export const OrdersList = () => {
  const dispatch = useDispatch();
  const [modalOpen, setModalOpen] = useState(false);
  const [formData, setFormData] = useState<OrderType>(defaultFormState);
  const [statusMessage, setStatusMessage] = useState<StatusMessageType>({
    type: null,
    message: "",
  });

  let orders = useSelector((state) => state.OrderReducer.orders);
  const loading = useSelector((state) => state.OrderReducer.loading);
  // We need the products list for our order form
  let products = useSelector((state) => state.ProductReducer.products);
  const productsLoading = useSelector((state) => state.ProductReducer.loading);

  useEffect(() => {
    (async () => {
      await dispatch(fetchOrders());
      await dispatch(fetchProducts());
    })();
  }, []);

  /*
   * We want to open the edit form, so fetch the product, set it as the forms state and open the modal.
   * We are not fetching the product from our local list because it could be outdated (someone else could have deleted/editted it already)
   */
  const OpenEditForm = async (id: number) => {
    try {
      let order: any = await dispatch(getOrder(id));
      setFormData(order);
      setModalOpen(true);
    } catch (e) {
      setStatusMessage({
        type: StatusMessages.DANGER,
        message: defaultApiCatchError,
      });
    }
  };

  /* Submit form logic. If there's an id then we're editting otherwise we're adding */
  const SubmitForm = async () => {
    try {
      if (formData.id != null) {
        await dispatch(editOrder(formData.id, formData));
        setStatusMessage({
          type: StatusMessages.SUCCESS,
          message: `Je hebt met succes de bestelling met ID "${formData.id}" aangepast!`,
        });
      } else {
        await dispatch(createOrder(formData));
        setStatusMessage({
          type: StatusMessages.SUCCESS,
          message: `Je hebt met succes een bestelling aangemaakt voor "${formData.customerName}"!`,
        });
      }

      setModalOpen(false);
      setFormData(defaultFormState);
    } catch (e) {
      setStatusMessage({
        type: StatusMessages.DANGER,
        message: defaultApiCatchError,
      });
    }
  };

  /* We want to delete the given order */
  const DeleteOrder = async (order: OrderType) => {
    try {
      if (order.id) {
        await dispatch(deleteOrder(order.id));
        setStatusMessage({
          type: StatusMessages.SUCCESS,
          message: `Je hebt met succes de bestelling met ID "${order.id}" verwijderd.`,
        });
      }
    } catch (e) {
      setStatusMessage({
        type: StatusMessages.DANGER,
        message: defaultApiCatchError,
      });
    }
  };

  return (
    <CrudPage
      item="bestelling"
      title="Bestellingen"
      loading={loading}
      defaultForm={defaultFormState}
      formData={formData}
      setFormData={(data: OrderType) => setFormData(data)}
      openEditForm={(id: number) => OpenEditForm(id)}
      formId={formId}
      form={
        <OrderForm
          form={formData}
          setFormData={setFormData}
          formId={formId}
          onSubmit={() => SubmitForm()}
          products={products.data}
          productsLoading={productsLoading}
        />
      }
      delete={(order: OrderType) => DeleteOrder(order)}
      listHeaders={[
        ORDER_FIELDS.id,
        ORDER_FIELDS.orderedAt,
        ORDER_FIELDS.customerName,
        ORDER_FIELDS.shippingAddress,
      ]}
      listData={orders.data}
      statusMessage={statusMessage}
      modalOpen={modalOpen}
      setModalOpen={(status: boolean) => setModalOpen(status)}
    />
  );
};

export default OrdersList;
