import { Fragment, useState } from "react";
import { ORDER_FIELDS, ORDER_PRODUCT_FIELDS } from "../../util/Fields";
import { Input } from "../Input";
import {
  IForm,
  OrderType,
  OrderProductsType,
  JsonApiDataProducts,
} from "../../Types";
import { ValidatingForm, requiredFieldText } from "../Formik";
import * as Yup from "yup";
import moment from "moment";
import { SuccessButton, DangerButton } from "../../util/Styling";

export const OrderForm = (props: IForm) => {
  const [formValidationErrors, setFormValidationErrors] = useState<any>(null);
  const { products } = props;

  const RemoveProductOrder = (index: number) => {
    const productOrder = [...props.form.products];
    productOrder.splice(index, 1);
    props.setFormData({ ...props.form, products: productOrder });
  };

  const SetProductOrder = (index: number, newObject: Record<string, any>) => {
    // Extra check to ensure that this index exists
    if (props.form.products[index] != null) {
      for (const [key, value] of Object.entries(newObject)) {
        let newProductOrders = props.form.products;
        newProductOrders[index] = {
          ...newProductOrders[index],
          [key]: value,
        };
        props.setFormData({ ...props.form, products: newProductOrders });
      }
    }
  };

  const Orders = () => {
    let i = 0;

    let orderedProducts = props.form.products.map(
      (order: OrderProductsType, index: number) => {
        i++;

        return (
          <div key={index} className="row">
            <div className="col-5">
              <Input
                column={ORDER_PRODUCT_FIELDS.productId}
                value={order.productId}
                selectOptions={products!.map((option: JsonApiDataProducts) => {
                  return option.attributes;
                })}
                onChange={(e: React.FormEvent<HTMLInputElement>) =>
                  SetProductOrder(index, {
                    productId: parseInt(e.currentTarget.value),
                  })
                }
                validationError={
                  formValidationErrors ? formValidationErrors.productId : ""
                }
              />
            </div>
            <div className="col-4">
              <Input
                column={ORDER_PRODUCT_FIELDS.quantity}
                value={order.quantity}
                onChange={(e: React.FormEvent<HTMLInputElement>) =>
                  SetProductOrder(index, {
                    quantity: parseInt(e.currentTarget.value),
                  })
                }
                validationError={
                  formValidationErrors ? formValidationErrors.quantity : ""
                }
              />
            </div>
            <div className="col-3" style={{ paddingTop: 32 }}>
              {i == 1 && (
                <SuccessButton
                  type="button"
                  icon="fas fa-plus"
                  onClick={() =>
                    props.setFormData({
                      ...props.form,
                      products: [
                        ...props.form.products,
                        { productId: 1, quantity: 0 },
                      ],
                    })
                  }
                />
              )}
              {i > 1 && (
                <DangerButton
                  type="button"
                  icon="fas fa-minus"
                  onClick={() => RemoveProductOrder(index)}
                />
              )}
            </div>
          </div>
        );
      }
    );

    return <Fragment>{orderedProducts}</Fragment>;
  };

  return (
    <ValidatingForm
      id={props.formId}
      onSubmit={() => props.onSubmit()}
      validationScheme={Yup.object().shape({
        orderedAt: Yup.string().required(requiredFieldText),
        customerName: Yup.string().required(requiredFieldText),
        shippingAddress: Yup.string().required(requiredFieldText),
      })}
      validationErrors={formValidationErrors}
      setValidationErrors={(formValidationErrors: OrderType) =>
        setFormValidationErrors(formValidationErrors)
      }
      inputValues={props.form}
    >
      <Fragment>
        <Input
          column={ORDER_FIELDS.orderedAt}
          value={props.form.orderedAt}
          onChange={(e: Date) =>
            props.setFormData({
              ...props.form,
              orderedAt: moment(e),
            })
          }
          validationError={
            formValidationErrors ? formValidationErrors.orderedAt : ""
          }
        />

        <Input
          column={ORDER_FIELDS.customerName}
          value={props.form.customerName}
          onChange={(e: React.FormEvent<HTMLInputElement>) =>
            props.setFormData({
              ...props.form,
              customerName: e.currentTarget.value,
            })
          }
          validationError={
            formValidationErrors ? formValidationErrors.customerName : ""
          }
        />

        <Input
          column={ORDER_FIELDS.shippingAddress}
          value={props.form.shippingAddress}
          onChange={(e: React.FormEvent<HTMLInputElement>) =>
            props.setFormData({
              ...props.form,
              shippingAddress: e.currentTarget.value,
            })
          }
          validationError={
            formValidationErrors ? formValidationErrors.shippingAddress : ""
          }
        />

        {Orders()}
      </Fragment>
    </ValidatingForm>
  );
};

export default OrderForm;
