import styled from "styled-components";

interface NavigationStylingProps {
  readonly open: boolean;
}

export const Header = styled.header`
  background-color: ${(props) => props.theme.colors.headerBg};
  height: 65px;
`;

export const Logo = styled.span`
  color: ${(props) => props.theme.colors.headerText};
  font-size: 22px;
  margin-left: 20px;
`;

export const StyledBurger = styled.button<NavigationStylingProps>`
  z-index: 2000;
  float: left;
  left: 2rem;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 2rem;
  height: 2rem;
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0;

  ${(props) =>
    props.open &&
    `
      position: absolute;
    `};

  &:focus {
    outline: none;
  }

  div {
    width: 2rem;
    height: 0.25rem;
    background: ${(props) =>
      props.open
        ? props.theme.colors.burgerMenuText
        : props.theme.colors.headerText};
    border-radius: 10px;
    transition: all 0.3s linear;
    position: relative;
    transform-origin: 1px;

    :first-child {
      transform: ${(props) => (props.open ? "rotate(45deg)" : "rotate(0)")};
    }

    :nth-child(2) {
      opacity: ${(props) => (props.open ? "0" : "1")};
      transform: ${(props) =>
        props.open ? "translateX(20px)" : "translateX(0)"};
    }

    :nth-child(3) {
      transform: ${(props) => (props.open ? "rotate(-45deg)" : "rotate(0)")};
    }
  }
`;

export const StyledMenu = styled.nav<NavigationStylingProps>`
  z-index: 1000;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: ${(props) => props.theme.colors.headerText};
  color: ${(props) => props.theme.colors.burgerMenuText};
  height: 100vh;
  text-align: left;
  padding: 2rem;
  position: absolute;
  top: 0;
  left: 0;
  transition: transform 0.3s ease-in-out;
  transform: ${(props) => (props.open ? "translateX(0)" : "translateX(-100%)")};

  @media (max-width: 576px) {
    width: 100%;
  }

  a {
    font-size: 2rem;
    text-transform: uppercase;
    padding: 2rem 0;
    font-weight: bold;
    letter-spacing: 0.5rem;
    color: ${(props) => props.theme.colors.burgerMenuText};
    text-decoration: none;
    transition: color 0.3s linear;

    @media (max-width: 576px) {
      font-size: 1.5rem;
      text-align: center;
    }

    &:hover {
      color: ${(props) => props.theme.colors.burgerMenuTextHover};
    }
  }
`;
