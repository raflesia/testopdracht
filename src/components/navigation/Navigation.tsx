import { useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { TOGGLE_DARKTHEME } from "../../redux/Reducers";
import { Link } from "react-router-dom";
import { useOnClickOutsideElement } from "../Hooks";
import { Header, StyledBurger, Logo, StyledMenu } from "./NavigationStyle";

interface NavigationProps {
  open: boolean;
  setOpen: Function;
}

const Navigation = ({ open, setOpen }: NavigationProps) => {
  const darkThemeEnabled = useSelector((state) => state.theme.darkThemeEnabled);
  const dispatch = useDispatch();
  const navigationNode = useRef<any>();

  // Add the listener hook that executes if the visitor clicks outside of the navigation node
  useOnClickOutsideElement(navigationNode, () => setOpen(false));

  return (
    <div ref={navigationNode}>
      <Header className="py-3 mb-4">
        <div className="container">
          <StyledBurger open={open} onClick={() => setOpen(!open)}>
            <div />
            <div />
            <div />
          </StyledBurger>
          <Logo>Flora Logistics</Logo>

          <StyledMenu open={open}>
            <Link to="/" onClick={() => setOpen(!open)}>
              <span role="img" aria-label="home">
                🏠
              </span>{" "}
              Home
            </Link>
            <Link to="/products" onClick={() => setOpen(!open)}>
              {" "}
              <span role="img" aria-label="home">
                🌻
              </span>
              Producten
            </Link>
            <Link to="/orders" onClick={() => setOpen(!open)}>
              {" "}
              <span role="img" aria-label="home">
                💰
              </span>
              Bestellingen
            </Link>

            <div>
              <input
                type="checkbox"
                checked={darkThemeEnabled}
                onChange={() => dispatch({ type: TOGGLE_DARKTHEME })}
              ></input>
              <span style={{ marginLeft: 5 }}>Gebruik het donkere thema</span>
            </div>
          </StyledMenu>
        </div>
      </Header>
    </div>
  );
};

export default Navigation;
