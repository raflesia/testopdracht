import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchProducts,
  getProduct,
  createProduct,
  editProduct,
  deleteProduct,
} from "../../redux/products/ProductActions";
import { ProductType, StatusMessageType, StatusMessages } from "../../Types";
import CrudPage, { defaultApiCatchError } from "../CrudPage";
import { PRODUCT_FIELDS } from "../../util/Fields";
import { ProductForm } from "./ProductForm";

let defaultFormState = {
  id: null,
  name: "",
  description: "",
  price: 0.0,
  stock: 0,
};
let formId = "add-or-edit-products";

export const ProductsList = () => {
  const dispatch = useDispatch();
  const [modalOpen, setModalOpen] = useState(false);
  const [formData, setFormData] = useState<ProductType>(defaultFormState);
  const [statusMessage, setStatusMessage] = useState<StatusMessageType>({
    type: null,
    message: "",
  });

  let products = useSelector((state) => state.ProductReducer.products);
  const loading = useSelector((state) => state.ProductReducer.loading);

  useEffect(() => {
    (async () => {
      await dispatch(fetchProducts());
    })();
  }, []);

  /*
   * We want to open the edit form, so fetch the product, set it as the forms state and open the modal.
   * We are not fetching the product from our local list because it could be outdated (someone else could have deleted/editted it already)
   */
  const OpenEditForm = async (id: number) => {
    try {
      let product: any = await dispatch(getProduct(id));
      setFormData(product);
      setModalOpen(true);
    } catch (e) {
      setStatusMessage({
        type: StatusMessages.DANGER,
        message: defaultApiCatchError,
      });
    }
  };

  /* Submit form logic. If there's an id then we're editting otherwise we're adding */
  const SubmitForm = async () => {
    try {
      if (formData.id != null) {
        await dispatch(editProduct(formData.id, formData));
        setStatusMessage({
          type: StatusMessages.SUCCESS,
          message: `Je hebt met succes het product "${formData.name}" aangepast!`,
        });
      } else {
        await dispatch(createProduct(formData));
        setStatusMessage({
          type: StatusMessages.SUCCESS,
          message: `Je hebt met succes het product "${formData.name}" toegevoegd!`,
        });
      }

      setModalOpen(false);
      setFormData(defaultFormState);
    } catch (e) {
      setStatusMessage({
        type: StatusMessages.DANGER,
        message: defaultApiCatchError,
      });
    }
  };

  /* We want to delete the given product */
  const DeleteProduct = async (product: ProductType) => {
    try {
      if (product.id) {
        await dispatch(deleteProduct(product.id));
        setStatusMessage({
          type: StatusMessages.SUCCESS,
          message: `Je hebt met succes het product "${product.name}" verwijderd.`,
        });
      }
    } catch (e) {
      setStatusMessage({
        type: StatusMessages.DANGER,
        message: defaultApiCatchError,
      });
    }
  };

  return (
    <CrudPage
      item="product"
      title="Producten"
      loading={loading}
      defaultForm={defaultFormState}
      formData={formData}
      setFormData={(data: ProductType) => setFormData(data)}
      openEditForm={(id: number) => OpenEditForm(id)}
      formId={formId}
      form={
        <ProductForm
          form={formData}
          setFormData={setFormData}
          formId={formId}
          onSubmit={() => SubmitForm()}
        />
      }
      delete={(product: ProductType) => DeleteProduct(product)}
      listHeaders={[
        PRODUCT_FIELDS.id,
        PRODUCT_FIELDS.name,
        PRODUCT_FIELDS.description,
        PRODUCT_FIELDS.price,
        PRODUCT_FIELDS.stock,
      ]}
      listData={products.data}
      statusMessage={statusMessage}
      modalOpen={modalOpen}
      setModalOpen={(status: boolean) => setModalOpen(status)}
    />
  );
};

export default ProductsList;
