import { Fragment, useState } from "react";
import { PRODUCT_FIELDS } from "../../util/Fields";
import { Input } from "../Input";
import { IForm, ProductType } from "../../Types";
import { ValidatingForm, requiredFieldText, numericFieldText } from "../Formik";
import * as Yup from "yup";

export const ProductForm = (props: IForm) => {
  const [formValidationErrors, setFormValidationErrors] = useState<any>(null);

  return (
    <ValidatingForm
      id={props.formId}
      onSubmit={() => props.onSubmit()}
      validationScheme={Yup.object().shape({
        name: Yup.string().required(requiredFieldText),
        description: Yup.string().required(requiredFieldText),
        price: Yup.number()
          .required(requiredFieldText)
          .notOneOf([0], "Een prijs van 0 euro is niet verstandig.")
          .min(0, "De prijs mag niet onder de 0 euro zijn.")
          .typeError(numericFieldText),
        stock: Yup.number()
          .required(requiredFieldText)
          .typeError(numericFieldText),
      })}
      validationErrors={formValidationErrors}
      setValidationErrors={(formValidationErrors: ProductType) =>
        setFormValidationErrors(formValidationErrors)
      }
      inputValues={props.form}
    >
      <Fragment>
        <Input
          column={PRODUCT_FIELDS.name}
          value={props.form.name}
          onChange={(e: React.FormEvent<HTMLInputElement>) =>
            props.setFormData({ ...props.form, name: e.currentTarget.value })
          }
          validationError={
            formValidationErrors ? formValidationErrors.name : ""
          }
        />

        <Input
          column={PRODUCT_FIELDS.description}
          value={props.form.description}
          onChange={(e: React.FormEvent<HTMLInputElement>) =>
            props.setFormData({
              ...props.form,
              description: e.currentTarget.value,
            })
          }
          validationError={
            formValidationErrors ? formValidationErrors.description : ""
          }
        />

        <Input
          column={PRODUCT_FIELDS.price}
          value={props.form.price}
          onChange={(e: React.FormEvent<HTMLInputElement>) =>
            props.setFormData({
              ...props.form,
              price: parseFloat(e.currentTarget.value),
            })
          }
          validationError={
            formValidationErrors ? formValidationErrors.price : ""
          }
        />

        <Input
          column={PRODUCT_FIELDS.stock}
          value={props.form.stock}
          onChange={(e: React.FormEvent<HTMLInputElement>) =>
            props.setFormData({
              ...props.form,
              stock: parseInt(e.currentTarget.value),
            })
          }
          validationError={
            formValidationErrors ? formValidationErrors.stock : ""
          }
        />
      </Fragment>
    </ValidatingForm>
  );
};

export default ProductForm;
