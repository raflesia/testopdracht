import { Fragment } from "react";
import { CrudPageType, ColumnType } from "../Types";
import { DangerButton, SuccessButton, StatusMessage } from "../util/Styling";
import { Modal } from "react-bootstrap";
import moment from "moment";

export const defaultApiCatchError =
  "Er is een probleem opgetreden. Kan de actie niet uitvoeren.";

/* Takes the headers of the table and matches it with the given data to create a row entry in the table */
const AddTableRow = (props: CrudPageType, row: any) => {
  // Create the row filled with our data first
  let renderRow = props.listHeaders.map((field: ColumnType) => {
    if (row[field.name] != null) {
      let value = row[field.name];
      if (field.type == "datepicker") {
        value = moment(row[field.name]).format("DD-MM-yyyy HH:mm:ss");
      }
      return <td key={field.name}>{value}</td>;
    } else {
      return <td>Error could not load value</td>;
    }
  });

  // We're going to append the actions column by default as this is a crud
  return (
    <Fragment>
      {renderRow}
      <td>
        <SuccessButton
          icon="fas fa-search"
          style={{ width: 50 }}
          onClick={() => {
            props.openEditForm(row.id);
          }}
        />
        <DangerButton
          icon="fas fa-times"
          style={{ width: 50, marginLeft: 10 }}
          onClick={() => {
            if (
              window.confirm(
                `Weet je zeker dat je ID ${row.id} wilt verwijderen?`
              )
            )
              props.delete(row);
          }}
        />
      </td>
    </Fragment>
  );
};

const CrudPage = (props: CrudPageType) => {
  // Keep track if our form is in it's edit state or not
  let isEditting: boolean = props.formData.id;
  let itemWithCapitalLetter =
    props.item.charAt(0).toUpperCase() + props.item.slice(1);

  let title = (
    <div className="row">
      <h3 style={{ paddingBottom: 20 }}>{props.title}</h3>
    </div>
  );

  // The list is still loading, so let's tell the user that we're still loading
  if (props.loading) {
    return (
      <Fragment>
        {title}
        <div className="row">Aan het laden...</div>
      </Fragment>
    );
  }

  // Do we want to open the add form? Then not only open the modal, but also reset the form to it's default state if were editting
  const OpenAddForm = () => {
    if (isEditting) {
      props.setFormData(props.defaultForm);
    }
    props.setModalOpen(true);
  };

  return (
    <Fragment>
      {title}

      <StatusMessage {...props.statusMessage} />

      <SuccessButton
        text={`${itemWithCapitalLetter} toevoegen`}
        icon="fas fa-plus"
        style={{ marginBottom: 15 }}
        onClick={() => {
          OpenAddForm();
        }}
      />

      <table className="table">
        <thead>
          <tr>
            {props.listHeaders.map((field: any) => (
              <th key={field.name}>{field.label}</th>
            ))}
            <th key="actions">Acties</th>
          </tr>
        </thead>
        <tbody>
          {props.listData.map((row: any) => {
            return <tr key={row.id}>{AddTableRow(props, row.attributes)}</tr>;
          })}
        </tbody>
      </table>

      <Modal show={props.modalOpen} onHide={() => props.setModalOpen(false)}>
        <Modal.Header closeButton>
          <Modal.Title>
            {isEditting
              ? `${itemWithCapitalLetter} bijwerken`
              : `Nieuw ${props.item}`}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-12">{props.form}</div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <SuccessButton
            text={isEditting ? "Bijwerken" : "Toevoegen"}
            icon={isEditting ? "fas fa-pencil-alt" : "fas fa-plus"}
            form={props.formId}
          />
          <DangerButton
            text="Annuleren"
            icon="fas fa-times"
            style={{ marginLeft: 15 }}
            onClick={() => {
              props.setModalOpen(false);
            }}
          />
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};
export default CrudPage;
