import React, { Fragment } from "react";
import { ProductsList } from "./products/ProductsList";
import { OrdersList } from "./orders/OrdersList";

const Home = () => {
  return (
    <Fragment>
      <ProductsList />
      <div style={{ marginTop: 40 }}>
        <OrdersList />
      </div>
    </Fragment>
  );
};

export default Home;
