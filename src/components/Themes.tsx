import { DefaultTheme } from "styled-components";

export const LightTheme: DefaultTheme = {
  name: "light-theme",

  colors: {
    bodyText: "#000",
    bodyBg: "#FCF6F4",

    headerText: "#16577a",
    headerBg: "#b7b6b6",

    burgerMenuText: "#fff",
    burgerMenuTextHover: "#5bc6fd",
  },
};

export const DarkTheme: DefaultTheme = {
  name: "dark-theme",

  colors: {
    bodyText: "#fff",
    bodyBg: "#292929",

    headerText: "#247faf",
    headerBg: "#111111",

    burgerMenuText: "#fff",
    burgerMenuTextHover: "#343078",
  },
};
