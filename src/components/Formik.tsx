import { Fragment } from "react";
import { Formik } from "formik";
import { ValidatingFormType, ValidatingFormErrorType } from "../Types";

export const requiredFieldText = "Dit veld is verplicht.";
export const numericFieldText =
  "Je mag hier alleen numerieke waardes invullen.";

export const validate = async (props: ValidatingFormType, values: any) => {
  const updateValidationErrors = (errors: Object) => {
    if (props.validationErrors !== errors) {
      props.setValidationErrors(errors);
    }
  };

  // Check the schema to decide if the form is valid:
  const isFormValid = await props.validationScheme.isValid(values, {
    abortEarly: false, // Prevent aborting validation after first error
  });

  // The form is valid so update the errors to be empty
  if (isFormValid) {
    const errors = {};
    updateValidationErrors(errors);
    return errors;
  } else {
    // If the form is not valid, check which fields are incorrect
    props.validationScheme
      .validate(values, { abortEarly: false })
      .catch((err: any) => {
        // Collect all errors in { fieldName: message } format:
        const errors = err.inner.reduce(
          (field: any, error: ValidatingFormErrorType) => {
            return {
              ...field,
              [error.path]: error.message,
            };
          },
          {}
        );

        // Give the errors back to the parent
        updateValidationErrors(errors);

        return errors;
      });
  }
};

export const ValidatingForm = (props: ValidatingFormType) => {
  return (
    <Formik
      validateOnChange={true}
      enableReinitialize={true}
      initialValues={props.inputValues}
      onSubmit={async (values) => {
        const formValid = await validate(props, values);
        if (formValid) {
          props.onSubmit();
        }
      }}
    >
      {({ handleSubmit }) => {
        return (
          <Fragment>
            <form id={props.id} method="post" onSubmit={handleSubmit}>
              {props.children}
            </form>
          </Fragment>
        );
      }}
    </Formik>
  );
};
