import 'react-redux';

import { AppState } from '../redux/Reducers';

declare module 'react-redux' {
  interface DefaultRootState extends AppState { };
}