import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    name: string,
    
    colors: {
        bodyText: string,
        bodyBg: string,

        headerText: string,
        headerBg: string;

        burgerMenuText: string;
        burgerMenuTextHover: string;
    };
  }
}