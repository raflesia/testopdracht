import * as actions from "./OrderActions";
import { OrderType, JsonApiDataOrders } from "../../Types";

type OrderReducerType = {
  loading: boolean;
  orders: any;
  getOrder: OrderType | null;
  error: string | null;
};

let initialProductsState = {
  loading: true,
  orders: [],
  getOrder: null,
  error: null,
};

const OrderDataState = (payload: JsonApiDataOrders) => {
  return {
    type: "orders",
    id: payload.id,
    attributes: payload,
  };
};

export const OrderReducer = (
  state: OrderReducerType = initialProductsState,
  action: any
) => {
  switch (action.type) {
    case actions.FETCH_ORDERS:
      return {
        ...state,
        loading: false,
        orders: action.payload,
      };
    case actions.GET_ORDER:
      return {
        ...state,
        getOrder: action.payload,
      };
    case actions.EDIT_ORDER:
      let orders = state.orders.data;
      // Find the index of the order that we've editted, and edit it in the products list
      let index = orders.findIndex((x) => x.id === action.payload.id);
      orders[index] = OrderDataState(action.payload);

      return {
        ...state,
        orders: { data: orders },
      };
    case actions.CREATE_ORDER:
      return {
        ...state,
        orders: {
          data: [...state.orders.data, OrderDataState(action.payload)],
        },
        payload: action.payload,
      };
    case actions.DELETE_ORDER:
      // We need to filter this product out of the list so create a new data state
      let newOrderData = state.orders.data.filter(
        (order: any) => order.id !== action.payload.id
      );

      return {
        ...state,
        orders: { data: newOrderData },
        payload: action.payload,
      };
    default:
      return state;
  }
};
