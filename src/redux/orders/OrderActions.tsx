import * as apiClient from "../../util/ApiClient";
import { OrderType } from "../../Types";

const defaultDispatch = (type: any, payload: any) => {
  return { type, payload };
};

export const FETCH_ORDERS = "FETCH_PRODUCTS_ORDERS";
export const GET_ORDER = "GET_ORDER";
export const EDIT_ORDER = "EDIT_ORDER";
export const CREATE_ORDER = "CREATE_ORDER";
export const DELETE_ORDER = "DELETE_ORDER";

export const fetchOrders = () => async (dispatch: any) => {
  try {
    const res = await apiClient.getAllOrders();

    // Create the JSON api format response
    let jsonApiResponse = {
      data: res.data.map((row: OrderType) => {
        return { type: "orders", id: row.id, attributes: row };
      }),
    };

    dispatch(defaultDispatch(FETCH_ORDERS, jsonApiResponse));
  } catch (err) {
    return Promise.reject(err);
  }
};
export const getOrder = (id: number) => async (dispatch: any) => {
  try {
    const res = await apiClient.getOrder(id);
    dispatch(defaultDispatch(GET_ORDER, res.data));
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};
export const editOrder =
  (id: number, form: OrderType) => async (dispatch: any) => {
    try {
      const res = await apiClient.editOrder(id, form);
      dispatch(defaultDispatch(EDIT_ORDER, res.data));
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };

export const createOrder = (form: OrderType) => async (dispatch: any) => {
  try {
    const res = await apiClient.createOrder(form);
    dispatch(defaultDispatch(CREATE_ORDER, res.data));
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};
export const deleteOrder = (id: number) => async (dispatch: any) => {
  try {
    const res = await apiClient.deleteOrder(id);
    dispatch(defaultDispatch(DELETE_ORDER, { id }));
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};
