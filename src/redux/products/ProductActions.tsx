import * as apiClient from "../../util/ApiClient";
import { ProductType } from "../../Types";

const defaultDispatch = (type: any, payload: any) => {
  return { type, payload };
};

/* Products actions */
export const FETCH_PRODUCTS = "FETCH_PRODUCTS_SUCCESS";
export const GET_PRODUCT = "GET_PRODUCT";
export const EDIT_PRODUCT = "EDIT_PRODUCT";
export const CREATE_PRODUCT = "CREATE_PRODUCT";
export const DELETE_PRODUCT = "DELETE_PRODUCT";

export const fetchProducts = () => async (dispatch: any) => {
  try {
    const res = await apiClient.getAllProducts();

    // Create the JSON api format response
    let jsonApiResponse = {
      data: res.data.map((row: ProductType) => {
        return { type: "products", id: row.id, attributes: row };
      }),
    };

    dispatch(defaultDispatch(FETCH_PRODUCTS, jsonApiResponse));
  } catch (err) {
    return Promise.reject(err);
  }
};
export const getProduct = (id: number) => async (dispatch: any) => {
  try {
    const res = await apiClient.getProduct(id);
    dispatch(defaultDispatch(GET_PRODUCT, res.data));
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};
export const editProduct =
  (id: number, form: ProductType) => async (dispatch: any) => {
    try {
      const res = await apiClient.editProduct(id, form);
      dispatch(defaultDispatch(EDIT_PRODUCT, res.data));
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };

export const createProduct = (form: ProductType) => async (dispatch: any) => {
  try {
    const res = await apiClient.createProduct(form);
    dispatch(defaultDispatch(CREATE_PRODUCT, res.data));
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};
export const deleteProduct = (id: number) => async (dispatch: any) => {
  try {
    const res = await apiClient.deleteProduct(id);
    dispatch(defaultDispatch(DELETE_PRODUCT, { id }));
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};
