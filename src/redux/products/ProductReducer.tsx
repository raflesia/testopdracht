import * as actions from "../products/ProductActions";
import { ProductType, JsonApiDataProducts } from "../../Types";

type ProductsReducerType = {
  loading: boolean;
  products: any;
  getProduct: ProductType | null;
  error: string | null;
};

let initialProductsState = {
  loading: true,
  products: { data: [] },
  getProduct: null,
  error: null,
};

const ProductDataState = (payload: JsonApiDataProducts) => {
  return {
    type: "products",
    id: payload.id,
    attributes: payload,
  };
};

export const ProductReducer = (
  state: ProductsReducerType = initialProductsState,
  action: any
) => {
  switch (action.type) {
    case actions.FETCH_PRODUCTS:
      return {
        ...state,
        loading: false,
        products: action.payload,
      };
    case actions.GET_PRODUCT:
      return {
        ...state,
        getProduct: action.payload,
      };
    case actions.EDIT_PRODUCT:
      let products = state.products.data;
      // Find the index of the product that we've editted, and edit it in the products list
      let index = products.findIndex((x) => x.id === action.payload.id);
      products[index] = ProductDataState(action.payload);

      return {
        ...state,
        products: { data: products },
      };
    case actions.CREATE_PRODUCT:
      return {
        ...state,
        products: {
          data: [...state.products.data, ProductDataState(action.payload)],
        },
        payload: action.payload,
      };
    case actions.DELETE_PRODUCT:
      // We need to filter this product out of the list so create a new data state
      let newProductData = state.products.data.filter(
        (product: any) => product.id !== action.payload.id
      );

      return {
        ...state,
        products: { data: newProductData },
        payload: action.payload,
      };
    default:
      return state;
  }
};
