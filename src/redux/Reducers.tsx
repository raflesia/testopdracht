import { combineReducers } from "redux";
import { ProductReducer } from "./products/ProductReducer";
import { OrderReducer } from "./orders/OrderReducer";

/* Too small to be put on it's own actions file */
export const TOGGLE_DARKTHEME = "TOGGLE_DARKTHEME";
export const toggleDarkTheme = () => {
  return {
    type: TOGGLE_DARKTHEME,
  };
};

/* Too small to be put on it's own reducer file */
const theme = (state: any = { darkThemeEnabled: false }, action: any) => {
  switch (action.type) {
    case TOGGLE_DARKTHEME:
      return { ...state, darkThemeEnabled: !state.darkThemeEnabled };
    default:
      return state;
  }
};

/* Setup */
export const reducers = combineReducers({
  theme,
  ProductReducer,
  OrderReducer,
});

export default reducers;
export type AppState = ReturnType<typeof reducers>;
