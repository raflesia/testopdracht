import axios from "axios";
import { ProductType, OrderType } from "../Types";

const axiosClient = axios.create({
  baseURL: "http://localhost:8000",
  headers: { "Content-Type": "application/json" },
});

export const getAllProducts = async () => {
  return await axiosClient.get("/products");
};
export const getProduct = async (id: number) => {
  return await axiosClient.get(`/products/${id}`);
};
export const editProduct = async (id: number, product: ProductType) => {
  return await axiosClient.put(`/products/${id}`, product);
};
export const createProduct = async (product: ProductType) => {
  return await axiosClient.post("/products", product);
};
export const deleteProduct = async (id: number) => {
  return await axiosClient.delete(`/products/${id}`);
};

export const getAllOrders = async () => {
  return await axiosClient.get("/orders");
};
export const getOrder = async (id: number) => {
  return await axiosClient.get(`/orders/${id}`);
};
export const editOrder = async (id: number, order: OrderType) => {
  return await axiosClient.put(`/orders/${id}`, order);
};
export const createOrder = async (order: OrderType) => {
  return await axiosClient.post("/orders", order);
};
export const deleteOrder = async (id: number) => {
  return await axiosClient.delete(`/orders/${id}`);
};
