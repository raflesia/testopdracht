export const PRODUCT_FIELDS = {
  id: {
    name: "id",
    label: "#",
    type: "text",
  },
  name: {
    name: "name",
    label: "Naam",
    type: "text",
  },
  description: {
    name: "description",
    label: "Omschrijving",
    type: "text",
  },
  price: {
    name: "price",
    label: "Prijs",
    type: "number",
  },
  stock: {
    name: "stock",
    label: "Voorraad",
    type: "number",
  },
};

export const ORDER_FIELDS = {
  id: {
    name: "id",
    label: "#",
    type: "text",
  },
  orderedAt: {
    name: "orderedAt",
    label: "Besteldatum",
    type: "datepicker",
  },
  customerName: {
    name: "customerName",
    label: "Klant",
    type: "text",
  },
  shippingAddress: {
    name: "shippingAddress",
    label: "Verzendadres",
    type: "text",
  },
};

export const ORDER_PRODUCT_FIELDS = {
  productId: {
    name: "id",
    label: "Product",
    type: "select",
    selectProps: {
      label: "name",
      value: "id",
    },
  },
  quantity: {
    name: "quantity",
    label: "Aantal",
    type: "number",
  },
};
