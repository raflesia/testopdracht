import { Fragment } from "react";
import { StatusMessageType, StatusMessages, StyledButtonType } from "../Types";

/*
 * Creates the <i> element with the given icon (fontawesome only).
 * Will automaticly use the fa class if fab, fas or far isn't present.
 */
export function Icon(icon: string, extraClass: string = "") {
  // Define the icon base class
  let iconClass = `${icon} ${extraClass}`;

  // Add fa if the icon doesn't start with far, fas or fab
  if (
    !icon.startsWith("far") &&
    !icon.startsWith("fas") &&
    !icon.startsWith("fab")
  ) {
    iconClass = "fa fa-" + iconClass;
  }

  return <i className={iconClass} />;
}

/*
 * Creates a Button using the StyledButtonType props
 */
export const Button = (props: StyledButtonType) => {
  let textMarginLeft = props.icon && props.text ? 10 : 0;
  let assignedProps: any = {
    className: `btn ${props.class}`,
    style: props.style,
  };

  if (props.onClick) {
    assignedProps = { ...assignedProps, onClick: props.onClick };
  }
  if (props.form) {
    assignedProps = { ...assignedProps, type: "submit", form: props.form };
  }
  if (props.type) {
    assignedProps = { ...assignedProps, type: props.type };
  }

  return (
    <button {...assignedProps}>
      {props.icon && Icon(props.icon)}
      <span style={{ marginLeft: textMarginLeft }}>{props.text}</span>
    </button>
  );
};

/*
 * Avoiding DRY: re-using the danger button
 */
export const DangerButton = (props: StyledButtonType) => {
  return <Button {...props} class="btn-danger" />;
};

/*
 * Avoiding DRY: re-using the success button
 */
export const SuccessButton = (props: StyledButtonType) => {
  return <Button {...props} class="btn-success" />;
};

/*
 * We want to show a message to the user after for example API interactions, which can be a good or a bad message.
 * Draw the message when one is present
 */
export const StatusMessage = (props: StatusMessageType) => {
  if (props.type == null) {
    return <Fragment />;
  }

  let title;
  let className;
  let renderIcon;

  if (props.type.valueOf() == StatusMessages.DANGER.valueOf()) {
    title = "Oeps, er ging wat verkeerd!";
    renderIcon = Icon("fas fa-ban");
    className = "alert alert-danger";
  } else if (props.type.valueOf() == StatusMessages.SUCCESS.valueOf()) {
    title = "Dat ging goed!";
    renderIcon = Icon("fas fa-check");
    className = "alert alert-success";
  } else {
    return <Fragment />;
  }

  return (
    <div className={className}>
      <h5>
        {renderIcon} {title}
      </h5>
      <p>{props.message}</p>
    </div>
  );
};
