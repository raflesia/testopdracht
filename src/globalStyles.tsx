import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
*,*::before,*::after,h1,h2,h3,h4,h5,h6{
    margin: 0;
    padding: 0;
}
h1,h2,h3,h4,h5,h6{
    display: inline-block;
}
body, html {
    height: 100%;
}
body {
    color: ${(props) => props.theme.colors.bodyText} !important;
    background-color: ${(props) => props.theme.colors.bodyBg} !important;
}

.form-label {
    font-weight: bold;
}

${(props) =>
  props.theme.name == "dark-theme" &&
  `
    .table {
        color: ${props.theme.colors.bodyText} !important;
    }

    .modal-content {
        background-color: ${props.theme.colors.bodyBg} !important;
    }

    .form-control {
        background-color: #898787 !important;
    }
  `};
`;

export default GlobalStyle;
