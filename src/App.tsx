import { Fragment, useState } from "react";
import { Route, Routes } from "react-router";
import { useSelector } from "react-redux";

// Theme & styling
import GlobalStyle from "./globalStyles";
import { ThemeProvider } from "styled-components";
import { LightTheme, DarkTheme } from "./components/Themes";

// Components
import Navigation from "./components/navigation/Navigation";
import Home from "./components/Home";
import ProductsList from "./components/products/ProductsList";
import OrdersList from "./components/orders/OrdersList";

function App() {
  const [open, setOpen] = useState(false);
  const darkThemeEnabled = useSelector((state) => state.theme.darkThemeEnabled);

  return (
    <Fragment>
      <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossOrigin="anonymous"
      />
      <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
        integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm"
        crossOrigin="anonymous"
      ></link>

      <ThemeProvider theme={darkThemeEnabled ? DarkTheme : LightTheme}>
        <GlobalStyle />
        <Navigation open={open} setOpen={setOpen} />

        <div className="container">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="products" element={<ProductsList />} />
            <Route path="orders" element={<OrdersList />} />
          </Routes>
        </div>
      </ThemeProvider>
    </Fragment>
  );
}

export default App;
